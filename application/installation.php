
create user 'bagheera'@'localhost' identified by 'kipling';
create database bagheera;
grant all privileges on bagheera.* to 'bagheera'@'localhost';

CREATE TABLE  IF NOT EXISTS scouts (
	id,
	nome VARCHAR(50),
	cognome VARCHAR(50),
	unita VARCHAR(2)
)

CREATE TABLE  IF NOT EXISTS guests (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50),
	cognome VARCHAR(50),
	bambino VARCHAR(2),
	pagato VARCHAR(2),
	entrato VARCHAR(2),
	uscito VARCHAR(2),
	invitatoDa 
)