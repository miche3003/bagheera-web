/*
|--------------------------------------------------------------------------
| QR Image Config
|--------------------------------------------------------------------------
|
| $config['qrcode_data_path'] is the path to the /data directory
| $config['qrcode_image_path'] is the path to the /image directory
| $config['qrcode_save_path'] is the directory to save generated images into
| 
| These paths should be full absolute directory paths.
| I have placed the /data and /image directories into the 'system/libraries'
| directory, but you should be able to put them anywhere setting the paths below.
|
| You must also make sure that the permissions on the 'qrcode_save_path'
| directory are set to 777 so that the server can create and save the images
| into that directory.
|
*/

$config['qrcode_data_path'] = '/srv/http/BagheeraWeb/application/libraries/data';
$config['qrcode_image_path'] = '/srv/http/BagheeraWeb/application/libraries/image';
$config['qrcode_save_path'] = '/srv/http/BagheeraWeb/public/img/qr/';
