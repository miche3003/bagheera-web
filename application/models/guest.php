<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Guest extends CI_Model {

    var $table = 'guests';
    var $qrc_dir = '/srv/http/BagheeraWeb/public/img/qr/';
    var $params = array('level' => 'H','size' => 5);
    var $KEYLENGHT = 15;
    
    function everybody (){
      $query = $this->db->get($this->table);
  		return $query;
	  }

    function by_id( $id ){
      $this->db->where("id", $id);
      return $this->db->get($this->table);
    }

    function by_host( $id ){
      $this->db->where("invitatoDa", $id);
      return $this->db->get($this->table);
    }

    function to_string ($r) {
      echo $r->nome . " ". $r->cognome . " " . $r->pagato . " " . " " . "<br>\n";
    }
    
    function generate_guest_key ($record) {
        $id = $record->id; 
        $invitatoDa = $record->invitatoDa; 
        $nome = $record->nome; 
        $cognome = $record->cognome;
        // $this->load->library('encrypt');
        // return $this->encrypt->encode( $id .":". $invitatoDa .":". $nome .":". $cognome);
        return substr(sha1($id . $invitatoDa . $nome . $cognome), 0, $this->KEYLENGHT);
    }
    
    function create( $nome, $cognome, $bambino, $pagato, $invitatoDa) {
        $data = array(
          'nome' => $nome,
          'cognome' => $cognome,
          'bambino' => ($bambino) ? 'SI' : 'NO',
          'pagato' => ($pagato) ? 'SI' : 'NO',
          'invitatoDa' => $invitatoDa
        );
         // Create the record
         $this->db->insert($this->table, $data);
        
         // Retrieve it
        $this->db->where('nome', $nome);
        $this->db->where('cognome', $cognome);
        $query = $this->db->get($this->table);

        //Create the key and insert it
        $res = $query->result();
        $k = $this->generate_guest_key($res[0]);
        $data = array( 'guest_key' => $k );
        $this->db->where('id', $res[0]->id);
        $this->db->update($this->table, $data);
    }

    function checkIn ($id) {
      $now = date('Y-m-d H:i:s'); // date("H:i d-n-y");
      $this->db->where('id',$id);
      $d = array('entrato' => $now);
      $this->db->update($this->table, $d);
    }

    // NOTA: filename è il path relativo a localhost/BagheeraWeb/public/img/qr/
    function add_qrcode($guest, $rw = false){

        $this->db->where('id', $guest);
        $r = $this->db->get($this->table)->result();
        $record = $r[0];
        
        if($record->qrcode_file == "" || ! $rw){

          $filename = $record->id . "_" . $record->cognome . "_" . $record->nome;
          $savename = $this->qrc_dir . $filename . '.png';
          if( ! file_exists($filename)){
                $this->load->library('ciqrcode');
                $this->params['data'] = $record->id . "-" . $record->guest_key;
                $this->params['savename'] = $savename;
                $this->ciqrcode->generate($this->params); 
           }
          
          $this->db->where('id', $guest);
          $data = array('qrcode_file' => $filename);
          $this->db->update( $this->table, $data);

        }
        return $record;

    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    // function gen_qrcodes (){

    // }

}

?> 