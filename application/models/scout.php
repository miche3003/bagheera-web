<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Scout extends CI_Model {

	var $table = 'scouts';

	function everybody (){
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function by_id ( $id ){
		$query = $this->db->where('id',$id)->get($this->table);
		return $query->result();
	}

	function by_name( $last, $first = null){
		$this->db->where('cognome',$last);
		if( isset($first))
			$this->db->where('nome',$first);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	public function to_string ($record) {
		$n = ucfirst($record->nome);
        $c = ucfirst($record->cognome);
        $u = $record->unita;
        $i = $record->id;
		return "${n} ${c} ${u} (${i})";
	}

	public function to_array( $record ){
		$a = array(
			'nome' => ucfirst($record->nome), 
			'cognome' => ucfirst($record->cognome),
			'unita' => $record->unita,
			'id' => $record->id
		);
		return $a;
	}
}

?> 