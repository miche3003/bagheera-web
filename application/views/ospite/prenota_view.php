<?php 
   $this->load->view('header_view'); 
?>

<script>

function new_line(e){
   var form = this.parentElement;
   form.style.setProperty('border', '1px solid black');
   return false;
}

</script>

<div id="containter">
   <h1>Aggiungi ospiti</h1>
   <h2>Scout:</h2>
   <form action="">
      Cognome: <input type="text" id="cognome-censito" />
      Nome: <input type="text" id="nome-censito" />
      Quante persone:
      <select>
         <?php for ($i = 1; $i <= 15; $i++): ?>
         <option><?php echo $i; ?></option>
         <?php endfor; ?> 
      </select>
      <input type="submit" onclick="censito"/>
   </form>
   <h2>Aggiungi ospiti in entrata</h2>
   <form action="" method="post">
      <div id="form_field">
       Nome : <input type="text" name="nome1" />
       Cognome : <input type="text" name="cognome1" />
       <select name="bambino1" >
         <option>Adulto</option>
         <option>Bambino</option>
       </select>
       Pagato <input type="checkbox" nama="pagato1"/>
       <button name="add" onclick="new_line"> + </button>
      </div>
      <input type="submit" />
   </form>
   <h2>Aggiungi ospiti che devono ancora arrivare</h2>
   <form>
      <div id="form_field">
       Nome : <input type="text" />
       Cognome : <input type="text" />
       <select >
         <option>Adulto</option>
         <option>Bambino</option>
       </select>
       Pagato <input type="checkbox" />
       <button name="add" onclick="new_line"> + </button>
      </div>
   </form>
</div>