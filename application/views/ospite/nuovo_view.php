<?php 
   $this->load->view('header_view'); 
?>

<script>
function new_line(e){
   var form = this.parentElement;
   form.style.setProperty('border', '1px solid black');
   return false;
}

function form_line(table, n ){
  var fields = [
    { 'name': 'nome' + n, 'type' : 'text'},
    { 'name': 'cognome' + n, 'type' : 'text'},
    { 'name': 'pagato' + n, 'type' : 'checkbox'},
    { 'name': 'bambino' + n, 'type' : 'checkbox'}
  ];
  var line = new Element('tr', { 'class': 'form_line', 'id' : 'line' + n});
  var cell = null;
  for(i=0;i<fields.length;i++){
    cell = new Element('td');
    cell.adopt(new Element('input', fields[i]));
    line.adopt(cell);
  }
  table.adopt(line);
}

function check_censito (){
  if( $("cognome-censito").value == ""){
    alert("Manca il cognome del censito!");
    return;
  }

  var obj = {
    last : $("cognome-censito").value, 
    first : $("nome-censito").value || undefined,
  }

  getScout( obj, function( response ){
    console.log(response);
    console.log("Eccoci");
    if ( response[1] != undefined ){
      var str = "Specificare il nome: ";
      for(var i in response)
        if( response.hasOwnProperty(i) )
          str = str + response[i].nome + "? ";
      alert(str);
      return;
    }

    if( response[0] == undefined){
      alert("nessuno scout trovato, errore di battitura?");
      return;
    }

    $("trova_scout").setStyle('display', 'none');
    var t = "";
    for( i in response[0])
      t = t + response[0][i] + " ";
    $("scout_host").set("text", t);
    $('scout_host_input').set('value', response[0].id);
    var howmany = $('howmany_input').get('value');
    $('howmany').set('value', howmany);
    for(var i = 1; i <= howmany; i++)
      form_line( $("form_field"), i);
    $("form_ospiti").adopt(new Element('input', {'type' : 'submit'}));
    $("div_form_ospiti").setStyle("display", "block");
  });
  
}

</script>

<div id="containter">
   <h1>Aggiungi ospiti</h1>
   <h2>Scout: <span id="scout_host" ></span></h2>
   <form id="trova_scout" action="#">
      Cognome: <input type="text" id="cognome-censito" />
      Nome: <input type="text" id="nome-censito" />
      Quante persone:
      <select id="howmany_input">
         <?php for ($i = 1; $i <= 15; $i++): ?>
         <option><?php echo $i; ?></option>
         <?php endfor; ?> 
      </select>
      <input type="button" value="Cerca" onclick="check_censito()"/>
   </form>
   <div id="div_form_ospiti" style="display:none">
     <h2>Aggiungi ospiti</h2>
     <form id="form_ospiti" action="add" method="post">
        <table id="form_field">
         <th class="labels">Nome</th><th class="labels">Cognome</th>
         <th class="labels">Pagato?</th><th class="labels">Bambino?</th>
         <input type="hidden" value="0" name="howmany" id="howmany"/>
         <input type="hidden" value="" name="scout_host" id="scout_host_input"/>
        </table>
     </form>
    </div>
</div>