<?php 
   $this->load->view('header_view'); 
?>

<h1>Invitati</h1>

<div>
	Filtra
	<form action=<?php echo "\"" . site_url('ospite/index/filter') ."\"" ?> method="post">
		Nome: Cognome: Entrato: Pagato:
		<input type="submit">
	</form>
</div>

<table>
	<tr>
		<th>Invitato</th>
		<th>Pagato?</th>
		<th>Bambino?</th>
		<th>Entrato</th>
		<th>Invitato da</th>
	<tr>
	<?php foreach ($invitati as $v) : ?>
		<tr>
			<td>
				<?php echo anchor( "ospite/show/" . $v->id, ucfirst($v->nome) . " " . ucfirst($v->cognome));  ?>
			</td>
			<td><?php echo $v->pagato; ?></td>
			<td><?php echo $v->bambino; ?></td>
			<td><?php echo $v->entrato; ?></td>
			<td class="scout_host">
				<a href="">
				<?php echo $v->invitatoDa; ?>
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

<div id="tooltip" style="display:none"> 
	<div id="tooltip_close" >
		<a href=""> x </a>
	</div>
	<span id="tooltip_content"> Attendi.. </span>
</div>

<script>

function tooltip (scout, x, y) {
	
	$('tooltip').setStyle('left', x);
	$('tooltip').setStyle('top', y);
	$('tooltip').setStyle('display', 'block');

	getScout( {id : scout} , function (res) {
		console.log(res);
		$('tooltip_content').set('text', res[0].nome + " " + res[0].cognome);
	});
}

$$('.scout_host').addEvent('click', function(e){
	tooltip(this.get('text').trim(), e.page.x, e.page.y );
	e.preventDefault();
	return -1;
});

$$('#tooltip_close>a')[0].addEvent('click', function(e){
	$('tooltip').setStyle('display', 'none');
	e.preventDefault();
});

</script>