<html>
	<head>
		<title>Biglietti</title>
		<style>
			@import url('http://localhost/BagheeraWeb/public/css/tickets.css');
		</style>
	</head>
	<body>
		<?php foreach ($guests as $key => $value): ?>
		<div class="biglietto">
			<img class="qrcode" src="http://localhost/BagheeraWeb/public/img/qr/<?php echo $value->qrcode_file ?>.png" />
			<div class="titolo">Sagra Lucca3 2012</div>
			<div class="nome"><?php echo ucfirst($value->nome) ?> <?php echo ucfirst($value->cognome) ?></div>
			<div class="ids">sid: <?php echo $value->invitatoDa ?> id: <?php echo $value->id ?></div>
		</div>
		<?php endforeach; ?>
	</body>
</html>