
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo isset($page_title) ? $page_title : "Bagheera Web"; ?></title>
	<link rel="stylesheet" type="text/css" href="http://localhost/BagheeraWeb/public/css/bagheera.css">
	<script type="text/javascript" 
		src="http://localhost/BagheeraWeb/public/js/mootools-core-1.4.5.js">
	</script>
	<script type="text/javascript" 
		src="http://localhost/BagheeraWeb/public/js/bagheera.js">
	</script>
</head>
<body>

<header>
	<div id="site_name">
		<?php echo anchor("/", "Bagheera Web", 'id="site_name"'); ?>
	</div>
	<nav>
	<?php echo anchor("ospite/", "Ospiti"); ?>
	| <?php echo anchor("ospite/prenota", "Prenotazioni"); ?>
	| <?php echo anchor("ospite/ingresso", "Ingresso"); ?>
	| <?php echo anchor("ticket", "Biglietti"); ?>
	</nav>
</header>
