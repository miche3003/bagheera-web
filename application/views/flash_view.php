<html>
	<head>
		<style>
			body {
				background-color: #444;
				font-family: "Arial";
			}
			.flash {
				border : 1px solid #999;
				box-shadow : 2px 2px 10px #111;
				width: 400px;
				margin: 50px auto 0 auto;
				padding: 20px 10px 40px 10px;
				background-color: #FFF;

				border-radius: 5px;
			}
			.message {
				font-size: 17pt;
			}

			.controls {
				text-align: right;
				margin-top: 50px;
			}

			.controls>a {
				border:1px solid #000;
				/* background-color: #33BB33; */
				color: #000;
				text-decoration: none;
				padding: 10px 5px 10px 5px;
				border-radius: 5px;
				background-image: linear-gradient(bottom, rgb(50,158,33) 0%, rgb(71,214,64) 16%, rgb(108,255,82) 79%);
background-image: -o-linear-gradient(bottom, rgb(50,158,33) 0%, rgb(71,214,64) 16%, rgb(108,255,82) 79%);
background-image: -moz-linear-gradient(bottom, rgb(50,158,33) 0%, rgb(71,214,64) 16%, rgb(108,255,82) 79%);
background-image: -webkit-linear-gradient(bottom, rgb(50,158,33) 0%, rgb(71,214,64) 16%, rgb(108,255,82) 79%);
background-image: -ms-linear-gradient(bottom, rgb(50,158,33) 0%, rgb(71,214,64) 16%, rgb(108,255,82) 79%);

			}
			.warn {
				margin: 45px 0 -30px 0;
				text-align :center;
			}
		</style>
	</head>
	<body>
		<div class="flash">
			<div class="message">
				<?php echo $message; ?>
			</div>
			<div class="controls">
				<?php echo $controls ?>
			</div>

		
		<?php if (isset($redirect)) : ?>
		<div class="warn">
			Stai per essere riportato alla pagina 
				<strong><?php echo anchor($redirect, $redirect); ?><strong>
		</div>
		<script src="<?php echo base_url(); ?>public/js/bagheera.js"></script>
		<script>
			// redirectDelay("<?php echo site_url($redirect); ?>");
		</script>
		<?php endif; ?>
		</div>
	</body>
</html>