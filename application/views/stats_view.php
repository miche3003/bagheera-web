<?php 
   $this->load->view('header_view'); 
?>

<div id="container">
	<h1>Home</h1>
	<table class="stats">
		<tr>
		    <td>Prenotati</td>
			<td>Entrati</td>
			<td>Mancanti</td>
		    <td>Incasso</td>
		</tr>
		<tr>
		    <td id="prenotati" class="results"><?php echo $prenotati; ?></td>
			<td id="entrati" class="results"><?php echo $entrati; ?></td>
			<td id="arrivandi" class="results"><?php echo $arrivandi; ?></td>
		    <td id="lordo" class="results"><?php echo $lordo; ?>€</td>
		</tr>
	</table>
	<div id="up_time"> 
	  aggiornato alle ore <span id="up_time"><?php echo date("H:i:s"); ?></span> di oggi
	</div>
</div>

<?php // todo : grafico e ajax auto update
?>
<script>
function update_stats(){
  var up_time = document.querySelector("span#up_time");
  var xhr = new XMLHttpRequest();
  var res = null;
  
  xhr.open("GET", "stats/json", true);  

  xhr.onreadystatechange = function(){
    res = xhr.responseText;
    res = JSON.parse( res );
    // console.log(res);
    var el = null;
    for(var i in res){
      // console.log(i + " "+ res[i]);
      el = document.querySelector("td#" + i);
      // console.log(el);
      el.innerHTML = res[i];
      if( i == "lordo")
        el.innerHTML = el.innerHTML + "€";
    }
  }

  xhr.send(null);
  
  up_time.innerHTML = (new Date()).toTimeString().substr(0,8);
}

setInterval(update_stats, 10 * 1000 );

</script>

</body>
</html>
