<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller {

	function index (){
		$this->load->view("stats_view", $this->get_stats());
	}
	
	function json (){
		$d = $this->get_stats();
		$res = "{ ";
		foreach( $d as $k => $v)
			$res = $res . "\"${k}\" : \"${v}\", ";
		$res = substr($res, 0, count($res) - 3);
		echo $res . " }";
	}
	
	private function get_stats (){
		
		$tutti = $this->db->count_all('guests');

		$this->db->where('entrato !=', 'NULL');
		$entrati = $this->db->count_all_results('guests');

		$this->db->where('bambino', 'SI');
		$this->db->where('pagato', 'SI');
		$bambini_pagati = $this->db->count_all_results('guests');

		$this->db->where('bambino', 'NO');
		$this->db->where('pagato', 'SI');
		$adulti_pagati  = $this->db->count_all_results('guests');


		$data['prenotati'] = $tutti;
		$data['entrati'] = $entrati;
		$data['arrivandi'] = $tutti - $entrati;
		$data['lordo'] = $adulti_pagati * 15 + $bambini_pagati * 10;
		
		return $data;
	}


}

?>
