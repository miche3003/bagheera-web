<?php

class Ticket extends CI_Controller {


	function index (){
	   $this->load->view('ticket_view');
	}

	function stampa($scout = -1, $guest = -1){
		
		$this->load->model('Guest');
		if( $guest != -1 ){
			$q = $this->Guest->by_id($guest);
		} else if( $scout != -1){
			$q = $this->Guest->by_host($scout);
		} else {
			$q = $this->Guest->everybody();
		}
		$data['guests'] = $q->result();
		$this->load->view("stampa_tickets_view", $data);
	}

	function addcode($guest){		
		$this->load->model('Guest');
		$record = $this->Guest->add_qrcode($guest);
		$msg = "Il file con il qr code &egrave; stato creato correttamente.<br>\n";
        $msg = $msg . "Ospite: " . $record->nome . " ". $record->cognome . " file: " . $record->qrcode_file . "<br>\n";
        $data['message'] = $msg;
        $data['controls'] =  anchor('ticket','Biglietti');
        $this->load->view('flash_view', $data);
	}

	function without_codes(){
		$this->db->where('qrcode_file', NULL);
		$r = $this->db->get('guests');
		foreach ($r->result() as $key => $value) {
			echo "{$value->nome} {$value->cognome}<br>\n";
		}
	}

}
