<?php

class Install extends CI_Controller {

        /*
        function create_db_tables(){


                        var $queries = array(
                                                
                        'corsi' => "CREATE TABLE IF NOT EXISTS corsi (          
	                        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	                        nome VARCHAR(50),
	                        descrizione VARCHAR(150),
	                        calendario TEXT
                        );",
                        
                        'avvisi' => "CREATE TABLE IF NOT EXISTS avvisi (
	                        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	                        titolo VARCHAR(30),
	                        contenuto TEXT
                        );"
                        
                        );

                        foreach($queries as $table => $q){
                                echo("-- Creating table ${table}");
                                $this->db->query( $query );
                        }
                        
        }
        
        function insert_dummy_entries(){
                        
                        var $queries = array(
                        'corsi' =>"# INSERT INTO corsi ( nome, descrizione, calendario)
                        #	VALUES ('Massaggio Thai 1', 'Corso di massaggio thailandese: intro',
                        #		'Dal 20 Settembre 2011 al 20 Dicembre 2011');",

                        'avvisi' => "INSERT INTO avvisi ( titolo, contenuto)
	                        VALUES ('Aperto il corso di Massaggio Thai1', 
		                        'Maggiori info nella pagina dei <a href=\"corsi\">corsi</a>');"
		        );
		        
        }


        function create_users_table(){
                
                var $query =                         
                          "CREATE TABLE IF NOT EXISTS `users` (
                          `user_id` int(10) unsigned NOT NULL auto_increment,
                          `user_email` varchar(255) NOT NULL default '',
                          `user_pass` varchar(60) NOT NULL default '',
                          `user_date` datetime NOT NULL default '0000-00-00 00:00:00',
                          `user_modified` datetime NOT NULL default '0000-00-00 00:00:00',
                          `user_last_login` datetime NULL default NULL,
                          PRIMARY KEY  (`user_id`),
                          UNIQUE KEY `user_email` (`user_email`)
                        ) DEFAULT CHARSET=utf8;";
                $this->db->query( $query );
                
        }

        */


        // create a new user
	function create_admin_user(){

		//	 *** Configure the user for login ***
		// the mail and password are to be set in the config.php file
		
		$this->load->library('SimpleLoginSecure');
		
		$user = $this->config->item('admin_user');
		$pw = $this->config->item('admin_pw');
		
		if ( ! ($user && $pw) ){
			echo("<p style=\"color:red\" > Name and");
			echo(" password are not set in the config.php file! Set them as ");
                       echo("${config['admin_user']} and ${config['admin_pw']} and rerun the install controller.</p>");
			return false;
		}
			
		if (  $this->simpleloginsecure->create( $user, $pw ) ) 
			echo( " <p>User $user created with password $pw</p>");
		else
			echo("<p style='color:red' >Error creating user $user with password $pw");
	}

        
        function index(){

		echo("<h1>Installation ..</h1>");

		/*
		echo("<h2>Create database tables ..</h3>"); $this->create_db_tables();
                echo("<h2>Insert dummy entries ..</h3>"); $this->create_db_tables();
                echo("<h2>Create <i>users</i> table ..</h3>"); $this->create_db_tables();
                */
                
		echo("<h2>Create admin user ..</h3>"); $this->create_admin_user();
		
	}
}

