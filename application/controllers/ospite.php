<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ospite extends CI_Controller {


	// visualizza tutti gli ospiti
	function index ( $f = "" ){
			
		if( $f == "filter" ){
			$data['invitati'] =  array();
			$this->load->view('ospite/index_view', $data);
		} else{
			$this->load->model('Guest');
			$data['invitati'] =  $this->Guest->everybody()->result();
			$this->load->view('ospite/index_view', $data);
		}
	}
	
	function show ( $id ){
		$this->load->model('Guest');
		$res =  $this->Guest->by_id($id)->result();
		$data['ospite'] = $res[0];

		$this->load->model('Scout');
		$res2 = $this->Scout->by_id( $res[0]->invitatoDa );

		$data['ospite']->invitatoDa = $res2[0]->nome . " " . $res2[0]->cognome;

		$this->load->view('ospite/show_view', $data);
	}

	function entra ( $enc = null ){
		if( is_null($enc) ){
			echo "manca id ospite";
			exit();
		}

		$this->load->model('Guest');

		$str = urldecode($enc);

		$a = explode("-",$str);

		$guest = $this->Guest->by_id($a[0])->result();
		if($guest[0]->guest_key != $a[1]){
			echo "errore nel biglietto. le chiavi non coincidono<br>";
			echo "Dettagli: arg: $str<br>";
			var_dump($a);
			var_dump($guest[0]);
			return;
		}

		if ( ! is_null($guest[0]->entrato) ){
			echo "ospite ". $a[0]. " gia' visto. Biglietto falso?";
			exit();
		} else {
			$this->Guest->checkIn($a[0]);
			echo "Benvenuto " . $guest[0]->nome . " " . $guest[0]->cognome;
		}

	}
	
	function prenota ( $scout = "", $quanti = 0 ){
		$this->load->view('nuovo_ospite_view');
	}

	function ingresso( $scout = "", $quanti = 0 ){
		$this->load->view('nuovo_ospite_view');
	}
	
	function add (){
		
		if ( ! isset($_POST['howmany']) ){
			echo "no howmany";
			return;
		}

		if ( ! isset($_POST['scout_host']) ){
			echo "no scout_host";
			return;
		}

		$howmany = $_POST['howmany'] + 0;
		echo "Inserisco " . $howmany . 
			( ( $howmany == 1) ? " invitato" : "invitati" ).   
			"per scout #" . $_POST['scout_host'] . "<br>\n";
		$msg = "";
		for( $i = 1; $i <= $howmany; $i++ ){
			$a = $this->read_nth_guest( $i );
			if ( $this->insert( $a ) != 0 ){
				$msg = "Errore nell'inserimento di :";
				foreach ($a as $key => $value) {
					$msg = $msg . " ${key} : ${value}";
				}
				$data['message'] = $msg;
				$data['controls'] = anchor('ospite/prenota', 'Ok');
				$this->load->view('flash_view',$data);
				return;
			}
			$msg = $msg . "Inserito correttamente ${i}<br>\n";
		}
		$msg = $msg . "Tutti gli invitati inseriti correttamente";
		$data['message'] = $msg;
		$data['controls'] = anchor('ospite', 'Ospiti');
		$this->load->view('flash_view',$data);
			
	}

	private function read_nth_guest ( $n ){
		
		$fields = array(
			'nome',
			'cognome',
			'bambino',
			'pagato',
			'scout_host'
		);
		
		$res = array();
		
		foreach ($fields as $value) {
			switch ($value) {
				case 'bambino' :
				case 'pagato' :
					if( ! isset($_POST[$value . $n])){
						$res[$value] = 'off';
					} else {
						$res[$value] = $_POST[ $value . $n];	
					}
					break;
				default:
					$v = $value . ( ($value == 'scout_host') ? "" : $n );
					if( ! isset($_POST[$v]) ){
						throw new Exception("Guest name or surname not inserted", 1);
					}
					$res[$value] = $_POST[$v];
			}
		}
		return $res;

	}


	/*
		Trasforma i valori inseriti nella form in valori da utilizzare nel db
		nome, cognome => stringhe
		pagato, bambno => booleani
		invitatoDa => id scout
	*/
	private function insert( $g ){
		foreach ($g as $key => $value) {
			if( $g[$key] == ""){
				return -1;
			}
		}
		
		$g['pagato'] = ($g['pagato'] == 'on') ? true : false;
		$g['bambino'] = ($g['bambino'] == 'on') ? true : false;
		$this->load->model('Guest');
		$this->Guest->create($g['nome'], $g['cognome'], 
							 $g['bambino'], $g['pagato'], $g['scout_host']);
		return 0;

	}
	
	function del ( $id ){
		$this->load->model('Guest');
		$r = $this->Guest->by_id($id)->result();
		$data['message'] = "Ospite {$r[0]->nome} {$r[0]->cognome} eliminato<br>\n";
		$this->Guest->delete($id);
		$data['controls'] = anchor('ospite', 'Tutti gli ospiti') . " " . anchor('', 'Annulla');
		$this->load->view('flash_view', $data);
	}

	function guestkey ( $id, $invitatoDa, $nome = "", $cognome = "") {

		$this->load->model('Guest');
		$this->load->library('encrypt');
		$res = $this->Guest->generate_guest_key( $id, $invitatoDa, $nome, $cognome );
		echo $res . "<br>\n";
		echo $this->encrypt->decode($res);

    }    

}

?>