<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Censito extends CI_Controller {
    
    function index ( $a = null, $b = null ){
        
        $r = $this->retrieve( $a, $b);
        $this->load->model('Scout');
		for ($i = 0; $i < count($r); $i++) {
            $v = $this->Scout->to_string($r[$i]);
		 	echo "$i : $v <br>\n";
		}
        
	}

    function json( $a = null, $b = null){
        $r = $this->retrieve($a, $b);
        $a = array();
        foreach ($r as $key => $value) {
            $a[$key] = $this->Scout->to_array($value);
        }
        echo json_encode($a, JSON_FORCE_OBJECT);
    }

    function retrieve ($a = null, $b = null ) {
        $this->load->model('Scout');
        if( isset($a) ){           
            if( intval($a) != 0)
                $res = $this->Scout->by_id( intval($a) );
            else{
                if( isset($b) )
                    $res = $this->Scout->by_name( $a, $b );
                else
                    $res = $this->Scout->by_name( $a );
            }
        } else {
            $res =  $this->Scout->everybody();
        }

        return $res;
    }

    function test_flash (){
        $data = array(
            'message' => 'Prova flash',
            'controls' => anchor('','Pagina Principale')
            //'redirect' => 'ospite'
        );
        $this->load->view('flash_view', $data);
    }
}

?>