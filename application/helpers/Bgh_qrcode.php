<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function qrcode_generate( $record ){

	var $qrc_dir = FCPATH . 'public/img/qr/';
	var $params = array(
		'level' => 'H',
		'size' => 5
	);
	
	$filename = $record->id . "_" . $record_cognome . "_" . $record_cognome;
	
	log_message( 'debug',"filename is:" . $filename);

	$savename = $this->qrc_dir . $filename . '.png';
	if( ! file_exists($filename)){
		$this->load->library('ciqrcode');
		$this->params['data'] = $record->guest_key;
		$this->params['savename'] = $savename;
		$this->ciqrcode->generate($this->params);	
	}
	return $filename;
}

function serve(){
	echo "ciao";
}

function test() {
  	$this->load->library('ciqrcode');

	$params['data'] = 'Un altro qr di prova';
	$params['level'] = 'H';
	$params['size'] = 5;
	$params['savename'] = FCPATH.'public/img/qr/test2.png';
	$this->ciqrcode->generate($params);
	echo "<h1>{$params['data']}</h1>";
	echo '<img src="'.base_url().'public/img/qr/test2.png" />';
}

function test2(){
 	$this->load->library('ciqrcode');

	header("Content-Type: image/png");
	$params['data'] = 'This is a text to encode become QR Code';
	$this->ciqrcode->generate($params);
 }

