#!/bin/bash

OUTDIR=out
INPUT=$1

# OPEN FILE
exec 3<$INPUT

I=1
while read -u 3 NAME DATA ; do
    echo "ITERAZIONE" $I
    let "I=$I+1"
# Crea un QR code con nome $NAME e contenente $DATA nella cartella $OUTDIR
    qrencode -o $OUTDIR/$NAME.png -m 6 -s 10 -l H $DATA
             # -m margini
             # -s dimensioni
             # -l correzione errori
done

echo "generati tutti i QR codes \n\n"

# unisce i QR codes generati in un unico file
    montage -label '%t' $OUTDIR/* # aggiunge un'etichetta col nome del file senza estensione