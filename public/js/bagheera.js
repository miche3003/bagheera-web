
/** Lets the Censito controller query the existance
*   of one or more scout in the db - AJAX/JSON
*   @arg who an object with the search criteria
		- id : db id
		- last : last name 
		(one of these is required)
		- first : first name (optional)
*   @arg cb  return function wich takes the JSON returned
*	Requires MooTools' Request.JSON
*/
function getScout( who, cb ) {

	var u = 'http://localhost/BagheeraWeb/censito/json/';
	
	if( who.id )
		u =  u + who.id;
	else {
		if( ! who.last ){
			throw new Error("getScout: invalid argument");
			return
		}
		u =  u + who.last;
		if( who.first )
			u = u + "/" + who.first;
	}
			
	var opts = {
		url : u,
		onSuccess : cb
	}

	var xhr = new Request.JSON( opts );
	xhr.send();

}

function uc_first (str) {
	return str[0].toUpperCase() + str.substr(1).toLowerCase();
}

function redirectDelay (r) {
	console.log('here');
	var f = (function (r){
		return function(){
			window.location = r;
		}
	})(r);
	setTimeout(f, 2000);
}